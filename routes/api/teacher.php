<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'teacher'], function () {

    Route::post('login', 'LoginController@login');
    Route::post('signUp', 'LoginController@signup');
    Route::post('token/refresh', 'LoginController@refreshToken');

    Route::group(['middleware' => ['auth:teacher-api']], function () {
        Route::get('getprofile', 'LoginController@get_profile');
        Route::post('updateprofile/detail', 'LoginController@update_profile');
    });
});
