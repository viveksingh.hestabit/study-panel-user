<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TeacherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'profile_image'=>$this->getUserDetails !=null ? $this->getUserDetails->profile : null,
            'address'=>$this->getUserDetails !=null ? $this->getUserDetails->address : null,
            'current_school'=>$this->getUserDetails !=null ? $this->getUserDetails->current_school : null,
            'previous_school'=>$this->getUserDetails !=null ? $this->getUserDetails->previous_school : null,
            'experience'=>$this->getTeacherDetails !=null ? $this->getTeacherDetails->experience : null,
            'expertise_subject'=>$this->getTeacherDetails !=null ? $this->getTeacherDetails->expertise_subject : null,

          ];
        // return parent::toArray($request);
    }
}
