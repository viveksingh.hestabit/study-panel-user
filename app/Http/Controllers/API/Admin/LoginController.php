<?php

namespace App\Http\Controllers\API\Admin;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\RefreshTokenRequest;

class LoginController extends Controller
{

    public function login(LoginRequest $request)
    {
        $admin = User::where('email', $request->email)->first();
        if ($admin) {
            if (Hash::check($request->password, $admin->password)) {
                $token = $this->_token($request->email, $request->password);
                if (isset($token['access_token'])) {
                    $adminData['name'] = $admin->name ? $admin->name : '';
                    return res_success('Success!', [
                        'admin'          =>  $adminData,
                        'token_type'    =>  $token['token_type'],
                        'expires_in'    =>  $token['expires_in'],
                        'access_token'  =>  $token['access_token'],
                        'refresh_token' =>  $token['refresh_token'],
                    ]);
                }
                return res(500, 'Server Error!');
            }
            return res_failed('Invalid password!');
        }
        return res_failed('Invalid user!');
    }

    public function refreshToken(RefreshTokenRequest $request)
    {
        $token = $this->_tokenRefresh($request->refresh_token);
        if (isset($token['access_token'])) {
            return res_success('Success!', [
                'token_type'    =>  $token['token_type'],
                'expires_in'    =>  $token['expires_in'],
                'access_token'  =>  $token['access_token'],
                'refresh_token' =>  $token['refresh_token'],
            ]);
        } elseif (count($token)) {
            return res_failed($token['message']);
        }
        return res(500, 'Server Error!');
    }

    private function _token($username, $password)
    {
        if ($client = $this->_client()) {
            $response = Http::asForm()->post(url('oauth/token'), [
                'grant_type' => 'password',
                'provider' => 'admins',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'username' => $username,
                'password' => $password,
                // 'scope' => '',
            ]);
            return $response->json();
        }
        return [];
    }

    private function _tokenRefresh($refresh_token)
    {
        if ($client = $this->_client()) {
            $response = Http::asForm()->post(url('oauth/token'), [
                'grant_type' => 'refresh_token',
                'provider' => 'admins',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'refresh_token' => $refresh_token,
                // 'scope' => '',
            ]);
            return $response->json();
        }
        return [];
    }

    private function _client()
    {
        return DB::table('oauth_clients')
            ->where('provider', 'admins')
            ->where('password_client', 1)
            ->first();
    }
}
